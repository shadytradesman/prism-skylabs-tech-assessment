"""
	Written for Python 3.4
	By Spencer Stecko
	3/5/2015
	Prism Skylabs tech assessment
"""

from geopy.geocoders import Nominatim #for looking up lat/lon of a given city name
from geopy.distance import vincenty #for taking the distance between two lat/lon coordinates
import sys #for handling command line arguments

###############################################
#
#			FUNCTION DEFINITIONS
#
###############################################

def find_lat_lon(placename):
	#placename is a string a la "New York, USA" formatting is not important
	#returns a string tuple in the form (lat, lon)
	#this function uses the Nominatim geocoder. Internet connection is required. 
	#Internet connection exceptions are not handled.
	geolocator = Nominatim()
	location = geolocator.geocode(placename)
	if not location or not location.latitude or not location.longitude:
		print("ERROR: FAILED TO FIND COORDINATES FOR CITY: " + placename)
		exit()
	return (location.latitude, location.longitude)

def distance_between_cities(placeA, placeB, units):
	#placeA and placeB are strings a la "new York, usa"
	locA = find_lat_lon(placeA)
	locB = find_lat_lon(placeB)
	if units == "mi":
		return vincenty(locA, locB).miles
	elif units == "km":
		return vincenty(locA, locB).kilometers

def get_distance_list(locationList, units):
	#given a list of locations, return a list of distances between them (length n-1)
	distances = []
	for idx, location in enumerate(locationList):
		if idx != 0:
			distances.append(distance_between_cities(location, locationList[idx-1], units))
	return distances
	
def print_itinerary(places,distances, units):
	#prints to std out an itinerary given a list of places, distances, and units (km or mi)
	print("Success! Your vacation itinerary is: \n")
	totalDis = 0
	for idx, place in enumerate(places):
		if idx !=0:
			print( "\t" + place + " -> " + places[idx-1] + ": " + "%.2f" % distances[idx-1] + " " +units)
			totalDis = totalDis + (distances[idx-1]) #keep a running total of distance
	print("\nTotal distance covered in your trip: %.2f" %totalDis + " " +units)

###############################################
#
#				BEGIN SCRIPT
#
###############################################

#Argument Handling
argset = set(sys.argv)
units = "mi"
optimize = False

if "-h" in argset or "help" in argset or "-help" in argset or "--help" in argset:
	print("Welcome to the itinerary distance calculator help menu")
	print("Accepted arguments:")
	print("-h, help, -help, --help: Shows this menu")
	print("-m: output in miles (default)")
	print("-k: output in kilometres")
	exit()
if "-k" in argset and "-m" in argset:
	print("ERROR: choose km or miles, not both")
	exit()
if "-k" in argset:
	units = "km"
if "-o" in argset:
	optimize = True

#Read Input
placeList = []
for line in sys.stdin.readlines():
	if line[-1] == '\n':
		placeList.append(line[:-1])
	else:
		placeList.append(line)
		
#Compute and Print Results
if not optimize:
	disList = get_distance_list(placeList, units)
	print_itinerary(placeList,disList, units)
else:
	"""Plan for optimizing route:
	
		Build completely connected graph with edge weights equal to distance between cities
		for each starting location find the minimum cost hamiltonian path
		This is bound to be terribly inefficient.
		"""
	print("feature not implemented")
	exit()